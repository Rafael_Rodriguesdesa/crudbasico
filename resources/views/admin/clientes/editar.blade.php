
@extends('layout.site')

@section('titulo','Editar Cliente')
    
@section('conteudo')

<div class="container">
    <h3 class="editar-texto text-center">Editando Cliente</h3>
    <div class="editar-formulario container-fluid text-center">
        <form action=" {{route('admin.clientes.atualizar',$registro->id)}} " method="post" enctype="multipart/form-data">  
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put">
                @include('admin.clientes._form')     
                <div class="btn-group pull-right" role="group" aria-label="Basic example">
                    <a href=" {{ route('admin.clientes') }} " class="btn btn-danger ">Cancelar</a>
                    <button class="btn btn-info">Atualizar</button>
                </div>
        </form>
    </div>
</div>

@endsection

@include('layout._includes.footer')

